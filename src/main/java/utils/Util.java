/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import sergey.bychkov.model.FondReport;
import sergey.bychkov.model.Transfer;

/**
 *
 * @author Sergey.Bychkov
 */
public class Util {

    public static String getMaxDate(List<Transfer> data) {
        Date date = null;
        if (data != null && data.size() > 0) {
            data.sort(Transfer.END);

            date = data.get(0).getTransferEnd();
        }
        return sdf(date);
    }

    public static String getMinDate(List<Transfer> data) {
        Date date = null;
        if (data != null && data.size() > 0) {
            data.sort(Transfer.START);

            date = data.get(0).getTransferStart();
        }
        return sdf(date);

    }

    public static String sdf(Date date) {
        String result = "";
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            result = sdf.format(date);
        }
        return result;
    }

    public static Double getFrTotal(List<FondReport> list) {
        double result = 0;
        if (list != null && !list.isEmpty()) {
            for (FondReport fr : list) {
                result += fr.getTotal();
            }
        }
        return result;
    }

    public static String getFrNums(List<FondReport> list) {
        return getFrNum(list).toString();
    }

    public static String getFrTotals(List<FondReport> list) {
        return new BigDecimal(getFrTotal(list)).setScale(2, RoundingMode.HALF_UP).toPlainString();
    }

    public static Integer getFrNum(List<FondReport> list) {
        int result = 0;
        if (list != null && !list.isEmpty()) {
            for (FondReport fr : list) {
                result += fr.getNum();
            }
        }
        return result;
    }

    private Util() {
    }
    
}
