/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.HorizontalAlignment;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import sergey.bychkov.model.FondReport;
import sergey.bychkov.model.Transfer;

/**
 *
 * @author Sergey.Bychkov
 */
public class PdfGenerator {

    private static final Log log = LogFactory.getLog(PdfGenerator.class);

    public static byte[] generate(String header, String footer, List<String[]> data, float[] sizes, PdfFont font) {

        ByteArrayOutputStream dest = new ByteArrayOutputStream();
        PdfWriter writer = new PdfWriter(dest);
        PdfDocument pdf = new PdfDocument(writer);
        Document document = new Document(pdf, PageSize.A4);
        //document.setMargins(20, 20, 20, 20);
        document.setFont(font);
        Paragraph pHeader = new Paragraph();
        pHeader.setHorizontalAlignment(HorizontalAlignment.CENTER);
        pHeader.add(new Text(header).setHorizontalAlignment(HorizontalAlignment.CENTER));

        document.add(pHeader);
        document.add(getTable(data, sizes));
        Paragraph pFooter = new Paragraph();
        pFooter.add(new Text(footer).setHorizontalAlignment(HorizontalAlignment.CENTER));
        pFooter.setHorizontalAlignment(HorizontalAlignment.CENTER);
        document.add(pFooter);

        document.close();

        return dest.toByteArray();
    }

    private static Table getTable(List<String[]> data, float[] sizes) {
        Table result = null;
        if (data != null && !data.isEmpty()) {
            int columns = data.get(0).length;
            result = sizes == null ? new Table(columns) : new Table(sizes);
            result.setHorizontalAlignment(HorizontalAlignment.CENTER);
            result.setWidthPercent(80);
            for (String[] row : data) {
                for (String cellText : row) {
                    if (cellText == null) {
                        cellText = " ";
                    }
                    result.addCell(new Cell().add(new Paragraph(cellText)
                            .setHorizontalAlignment(HorizontalAlignment.RIGHT)));
                }
            }
        }
        return result;
    }

    public static byte[] generateFond(List<FondReport> data, List<Transfer> trans) {
        PdfFont font = null;
        try {
            font = PdfFontFactory.createFont("c:\\windows\\fonts\\arial.ttf", "Cp1251", true);

        } catch (IOException ex) {
            Logger.getLogger(PdfGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        String startDate = Util.getMinDate(trans);
        String endDate = Util.getMaxDate(trans);
        //CHECK
        if (startDate.isEmpty() || endDate.isEmpty()) {
            log.debug("Dates are wrong \n start:" + startDate
                    + "\n end:" + endDate
                    + "\n transfers:" + trans);
        }
        //
        String header = "Справка по перечислениям Фонда капитального ремонта МКД\n за период с "
                + startDate + "  по " + endDate + "\n\n";
        String footer = "\nОтветственное лицо: _______________/__________________________/  "
                + Util.sdf(new Date());
        List<String[]> list = new ArrayList<>();
        list.add(new String[]{"Расчетный счет", "Кол-во", "Сумма"});
        if (data != null && !data.isEmpty()) {
            data.forEach((fr) -> {
                list.add(fr.getStrings());
            });
        }
        list.add(new String[]{"Итого", Util.getFrNums(data),
            Util.getFrTotals(data)});
        return generate(header, footer, list, new float[]{3, 1, 1}, font);
    }

    private PdfGenerator() {
    }
}
