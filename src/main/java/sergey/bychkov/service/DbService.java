package sergey.bychkov.service;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Service;
import sergey.bychkov.model.Partner;
import sergey.bychkov.model.PaymentData;
import sergey.bychkov.model.PostOffice;
import sergey.bychkov.model.Transfer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Sergey.Bychkov
 */
@Service
public class DbService {

    private JdbcTemplate template;
    @Value("${connectinString}")
    private String connectinString;

    @PostConstruct
    public void init() {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.firebirdsql.jdbc.FBDriver.class);
        dataSource.setUsername("sysdba");
        dataSource.setUrl(connectinString);
        dataSource.setPassword("masterkey");
        template = new JdbcTemplate(dataSource);
    }

    public List<Partner> getPartner() {
        List<Partner> result = template.query("select id, name from partner",
                new BeanPropertyRowMapper<Partner>(Partner.class));
        return result;
    }

    public List<PostOffice> getPostOffice() {
        List<PostOffice> result = template.query("select id, name from mainpostoffice",
                new BeanPropertyRowMapper<>(PostOffice.class));
        return result;
    }

    public List<PaymentData> getData(Long postOffice, Long partner) {
        String p1 = partner == null ? "855" : partner.toString();
        String p2 = postOffice == null ? "16" : postOffice.toString();
        String query = "select p.fcnumber, p.total, p.transfer_id "
                + "from payments p where p.partner_id= ? and p.mainpostoffice_id= ?";

        List<PaymentData> result = template.query(query, new Object[]{p1, p2},
                new BeanPropertyRowMapper<PaymentData>(PaymentData.class));
        return result;
    }

    public List<Transfer> getTransfer(Long postOffice, Long partner) {
        String p1 = partner == null ? "855" : partner.toString();
        String p2 = postOffice == null ? "16" : postOffice.toString();
        String query = "select id, mainpostoffice_id, partner_id, transfer_date,"
                + " transfer_start,transfer_end from transfers t "
                + "where t.partner_id= ? and t.mainpostoffice_id= ? order by id desc";

        List<Transfer> result = template.query(query, new Object[]{p1, p2},
                new BeanPropertyRowMapper<Transfer>(Transfer.class));
        return result;
    }

    List<PaymentData> getData(Long poId, Long paId, Long id) {
        String p1 = paId == null ? "855" : paId.toString();
        String p2 = poId == null ? "16" : poId.toString();
        String p3 = id == null ? "" : id.toString();
        String query = "select p.fcnumber, p.total, p.transfer_id "
                + "from payments p where p.partner_id= ? "
                + "and p.mainpostoffice_id= ? and p.transfer_id= ?";

        List<PaymentData> result = template.query(query, new Object[]{p1, p2, p3},
                new BeanPropertyRowMapper<PaymentData>(PaymentData.class));
        return result;
    }

}
