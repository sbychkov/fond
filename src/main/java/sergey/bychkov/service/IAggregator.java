/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.service;

import java.util.List;
import sergey.bychkov.model.FondReport;
import sergey.bychkov.model.Partner;
import sergey.bychkov.model.PostOffice;
import sergey.bychkov.model.Transfer;

/**
 *
 * @author Sergey.Bychkov
 */
public interface IAggregator {

    List<FondReport> getData(Long po, Long pa);

    List<FondReport> getData();

    List<FondReport> getData(Long poId, Long paId, List<Transfer> transfers);

    List<Partner> getPartner();

    List<PostOffice> getPostOffice();

    List<Transfer> getTransfers();

    List<Transfer> getTransfers(Long po, Long pa);
    
}
