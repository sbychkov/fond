/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sergey.bychkov.model.FondReport;
import sergey.bychkov.model.Partner;
import sergey.bychkov.model.PaymentData;
import sergey.bychkov.model.PostOffice;
import sergey.bychkov.model.Transfer;

/**
 *
 * @author Sergey.Bychkov
 */
@Service
public class Aggregator implements IAggregator {

    @Autowired
    private CsvService csv;
    @Autowired
    private DbService db;

    private final Logger log = LoggerFactory.getLogger(Aggregator.class);

    @Override
    public List<FondReport> getData(Long po, Long pa) {
        HashMap<String, FondReport> rsMap;
        rsMap = new HashMap<>();
        List<PaymentData> pd = db.getData(po, pa);
        pd.stream().forEach((pdRow) -> {
            String rs = csv.get(pdRow.getFcnumber());
            FondReport fr = rsMap.get(rs);
            if (fr != null) {
                fr.add(pdRow.getTotal());
            } else {
                rsMap.put(rs, new FondReport(rs, pdRow.getTotal()));
            }
        });
        return new ArrayList<>(rsMap.values());

    }

    @Override
    public List<FondReport> getData() {
        return getData(null, null);
    }

    @Override
    public List<Partner> getPartner() {
        return db.getPartner();
    }

    @Override
    public List<PostOffice> getPostOffice() {
        return db.getPostOffice();
    }

    @Override
    public List<Transfer> getTransfers() {
        return db.getTransfer(null, null);
    }

    @Override
    public List<Transfer> getTransfers(Long po, Long pa) {
        return db.getTransfer(po, pa);
    }

    @Override
    public List<FondReport> getData(Long poId, Long paId, List<Transfer> transfers) {
        final List<FondReport> result = new ArrayList<>();
        final HashMap<String, FondReport> rsMap = new HashMap<>();
        for (Transfer tr : transfers) {
            List<PaymentData> pd = db.getData(poId, paId, tr.getId());
            pd.stream().forEach((pdRow) -> {
                String rs = csv.get(pdRow.getFcnumber());
                FondReport fr = rsMap.get(rs);
                if (fr != null) {
                    fr.add(pdRow.getTotal());
                } else {
                    rsMap.put(rs, new FondReport(rs, pdRow.getTotal()));
                }
            });
            
        }
        result.addAll(new ArrayList<>(rsMap.values()));
        
/*        List<PaymentData> pd = new ArrayList<>();
        transfers.forEach((tr) -> {
            pd.addAll(db.getData(poId, paId, tr.getId()));
        });
        pd.stream().forEach((pdRow) -> {
            String rs = csv.get(pdRow.getFcnumber());
            FondReport fr = rsMap.get(rs);
            if (fr != null) {
                fr.add(pdRow.getTotal());
            } else {
                rsMap.put(rs, new FondReport(rs, pdRow.getTotal()));
            }
        });
        result.addAll(new ArrayList<>(rsMap.values()));
*/
        return result;
    }
}
