/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sergey.Bychkov
 */
@Service
public class CsvService {

    private State currentState = State.NEW;

    private HashMap<String, String> lsMap;

    private final Logger log = LoggerFactory.getLogger(CsvService.class);
    private boolean startWatcher = true;
    
    @Value("${fkrcsvfilename}")
    String fileName;

    public CsvService() {

    }

    public void setWatcher(boolean startWatcher) {
        this.startWatcher = startWatcher;
    }

    private void load() {
        setState(State.LOADING);
        lsMap = new HashMap<>();
        BufferedReader br = null;
        FileReader fr = null;
        try {

            fr = new FileReader(fileName);

            br = new BufferedReader(fr);
            String str;
            br.readLine();
            while ((str = br.readLine()) != null) {
                String[] s = str.split(";");
                if (s.length > 1) {
                    lsMap.put(s[1], s[0]);
                }
            }
            log.info("FKR.CSV was successfuly loaded");
        } catch (FileNotFoundException ex) {
            log.error(ex.getLocalizedMessage());
        } catch (IOException ex) {
            log.error(ex.getLocalizedMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException ex) {
                log.error(ex.getLocalizedMessage());
            }
        }

        setState(State.READY);
    }

    public String get(String key) {
        String result = null;
        if (currentState.equals(State.NEW)) {
            load();
        }
        if (currentState.equals(State.READY)) {
            result = lsMap.get(key);
        }
        log.debug("Value for key " + key + " is " + result);
        return result;
    }

    public State getState() {
        return currentState;
    }

    private void setState(State state) {
        currentState = state;
    }

    /*private String getFileName() {
        String filename = "c:\\temp\\fkr.csv";
        if (!new File(filename).exists()) {
            filename = "d:\\fkr\\fkr.csv";
        }
        return filename;
    }*/

    private Path getPath() {
        return Paths.get(fileName);
    }

    @PostConstruct
    private void watcherSetup() {
        Thread t = new Thread(new Watcher(), "Watcher");
        t.start();
    }

    public enum State {
        NEW, ERROR, LOADING, READY
    }

    private class Watcher implements Runnable {

        @Override
        public void run() {
            log.info("Starting watcher");
            try {
                WatchService watchService = FileSystems.getDefault().newWatchService();
                Path fkrPath = getPath();
                fkrPath.getParent().register(watchService,
                        StandardWatchEventKinds.ENTRY_MODIFY,
                        StandardWatchEventKinds.ENTRY_DELETE);
                while (startWatcher) {
                    WatchKey key = watchService.take();
                    for (WatchEvent<?> event : key.pollEvents()) {
                        log.info(((Path) event.context()).toString());
                        WatchEvent.Kind kind = event.kind();
                        if (kind.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
                            log.info("FKR.CSV was changed. Reloading.");
                            load();
                        }
                        boolean valid = key.reset();
                        if (!valid) {
                            break;
                        }
                    }
                }
            } catch (InterruptedException | IOException ex) {
                log.error(ex.getLocalizedMessage());
            }
        }
    }
}
