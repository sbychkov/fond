/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Sergey.Bychkov
 */
public class FondReport {

    private String rs;
    private Double total;
    private Integer num = 0;

    public FondReport(String rs, Double total) {
        this.rs = rs;
        this.total = total;
        this.num = 1;
    }

    public String[] getStrings() {
        String[] result = new String[3];
        result[0] = rs == null ? "" : rs;
        result[1] = getNumS();
        result[2] = getTotalS();
        return result;
    }

    /**
     * @return the rs
     */
    public String getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(String rs) {
        this.rs = rs;
    }

    /**
     * @return the total
     */
    public Double getTotal() {
        return total;
    }

    public String getTotalS() {
        return new BigDecimal(total).setScale(2, RoundingMode.HALF_UP).toPlainString();
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return the num
     */
    public Integer getNum() {
        return num;
    }

    public String getNumS() {
        return num.toString();
    }

    /**
     * @param num the num to set
     */
    public void setNum(Integer num) {
        this.num = num;
    }

    public void add(Double sum) {
        ++num;
        total += sum;
    }

}
