/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.model;

/**
 *
 * @author Sergey.Bychkov
 */
public class PaymentData {
    private String fcnumber;
    private Double total;
    private Long transferId;

    public String getFcnumber() {
        return fcnumber;
    }

    public void setFcnumber(String fcnumber) {
        this.fcnumber = fcnumber;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Long getTransferId() {
        return transferId;
    }

    public void setTransferId(Long transferId) {
        this.transferId = transferId;
    }
}
