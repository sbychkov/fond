/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.model;

import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import utils.Util;

/**
 *
 * @author Sergey.Bychkov
 */
public class Transfer implements Comparable<Transfer> {

    public final static Comparator<Transfer> START = new Comparator<Transfer>() {
        @Override
        public int compare(Transfer o1, Transfer o2) {
            return o1.getTransferStart().compareTo(o2.getTransferStart());
        }
    };
    public final static Comparator<Transfer> END = new Comparator<Transfer>() {
        @Override
        public int compare(Transfer o1, Transfer o2) {
            return o2.getTransferEnd().compareTo(o1.getTransferEnd());
        }
    };

    private Long id;
    private Long mainPostOfficeId;
    private Date transferDate;
    private Date transferStart;
    private Date transferEnd;
    private Long partnerId;

    public Transfer() {
    }

    public Transfer(Long id, Date start, Date end) {
        this.id = id;
        this.transferStart = start != null ? (Date) start.clone() : null;
        this.transferEnd = end != null ? (Date) end.clone() : null;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the mainPostOfficeId
     */
    public Long getMainPostOfficeId() {
        return mainPostOfficeId;
    }

    /**
     * @param mainPostOfficeId the mainPostOfficeId to set
     */
    public void setMainPostOfficeId(Long mainPostOfficeId) {
        this.mainPostOfficeId = mainPostOfficeId;
    }

    /**
     * @return the transferDate
     */
    public Date getTransferDate() {
        return (Date) transferDate.clone();
    }

    /**
     * @param transferDate the transferDate to set
     */
    public void setTransferDate(Date transferDate) {
        this.transferDate = (Date) transferDate.clone();
    }

    /**
     * @return the transferStart
     */
    public Date getTransferStart() {
        return (Date) transferStart.clone();
    }

    /**
     * @param transferStart the transferStart to set
     */
    public void setTransferStart(Date transferStart) {
        this.transferStart = (Date) transferStart.clone();
    }

    /**
     * @return the transferEnd
     */
    public Date getTransferEnd() {
        return (Date) transferEnd.clone();
    }

    /**
     * @param transferEnd the transferEnd to set
     */
    public void setTransferEnd(Date transferEnd) {
        this.transferEnd = (Date) transferEnd.clone();
    }

    /**
     * @return the partnerId
     */
    public Long getPartnerId() {
        return partnerId;
    }

    @Override
    public String toString() {
        return id + " от " + Util.sdf(transferDate)
                + " с " + Util.sdf(transferStart)
                + " по " + Util.sdf(transferEnd);
    }

    @Override
    public int compareTo(Transfer o) {
        int result = 0;
        if (o.id > this.id) {
            result = 1;
        } else if (o.id < this.id) {
            result = -1;
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.mainPostOfficeId);
        hash = 37 * hash + Objects.hashCode(this.transferDate);
        hash = 37 * hash + Objects.hashCode(this.transferStart);
        hash = 37 * hash + Objects.hashCode(this.transferEnd);
        hash = 37 * hash + Objects.hashCode(this.partnerId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transfer other = (Transfer) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.mainPostOfficeId, other.mainPostOfficeId)) {
            return false;
        }
        if (!Objects.equals(this.transferDate, other.transferDate)) {
            return false;
        }
        if (!Objects.equals(this.transferStart, other.transferStart)) {
            return false;
        }
        if (!Objects.equals(this.transferEnd, other.transferEnd)) {
            return false;
        }
        if (!Objects.equals(this.partnerId, other.partnerId)) {
            return false;
        }
        return true;
    }

}
