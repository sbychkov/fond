package sergey.bychkov;

import com.vaadin.spring.annotation.EnableVaadin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 *
 * @author Sergey.Bychkov
 */
@SpringBootApplication
@EnableVaadin
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class FondApplication {

	public static void main(String[] args) {
		SpringApplication.run(FondApplication.class, args);
	}
}
