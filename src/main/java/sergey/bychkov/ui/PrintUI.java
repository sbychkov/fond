/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import java.util.List;
import sergey.bychkov.model.FondReport;

/**
 *
 * @author Sergey.Bychkov
 */
@SpringUI(path="print")
@Theme("valo")
public class PrintUI extends UI {
        private static final long serialVersionUID = -4265213983602980250L;
    
       
        
        @Override
        protected void init(VaadinRequest request) {
            
            setContent(new Label(gen(),
                ContentMode.HTML));
            JavaScript.getCurrent().execute(
                "setTimeout(function() {" +
                "  print(); self.close();}, 2000);");
        }       
        
        private String gen(){
           StringBuilder result =new StringBuilder("Hi!");
           result.append("<table>");
            List<FondReport> data = (List<FondReport>) VaadinSession.getCurrent().getAttribute("data");
           
            data.forEach((fr)->{
            result.append("<tr>").append("<td>").append(fr.getRs()).append("/td")
                    .append("<td>").append(fr.getNumS()).append("</td>")
                    .append("<td>").append(fr.getTotalS()).append("</td>")
                    .append("</tr>");
            });
            result.append("</table");
            return result.toString();
        }
        
        
    }
