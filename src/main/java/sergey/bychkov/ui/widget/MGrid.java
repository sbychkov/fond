/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.ui.widget;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import java.util.ArrayList;
import java.util.List;
import sergey.bychkov.model.FondReport;
import utils.Util;

/**
 *
 * @author Sergey.Bychkov
 */
public class MGrid extends Grid {

    private static final long serialVersionUID = 1L;

    public MGrid() {
        super();
    }

    private void setCount(String txt) {
        getFooterRow(0).getCell("num").setText(txt);
    }

    private void setTotal(String txt) {
        getFooterRow(0).getCell("totalS").setText(txt);

    }

    public void setData(List<FondReport> data) {
        setColumns("rs", "num", "totalS");
        BeanItemContainer<FondReport> c = new BeanItemContainer<>(FondReport.class, data);
        setContainerDataSource(c);
        setWidth("100%");
        setCaption("Суммы по счетам");

        setColumnOrder("rs", "num", "totalS");
        getColumn("num").setHeaderCaption("Количество");
        getColumn("rs").setHeaderCaption("Расчетный счет");
        getColumn("totalS").setHeaderCaption("Сумма");
        setFooterVisible(true);
        appendFooterRow().getCell("rs").setText("Итого");
        setTotal(Util.getFrTotals(data));
        setCount(Util.getFrNums(data));
    }

    public List<FondReport> getGridData() {
        List<FondReport> result = new ArrayList<>();
        for (Object id : getContainerDataSource().getItemIds()) {
            result.add((FondReport) getContainerDataSource().getItem(id));
        }
        return result;
    }

}
