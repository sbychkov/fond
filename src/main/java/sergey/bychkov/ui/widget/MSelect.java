/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.ui.widget;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.TwinColSelect;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import sergey.bychkov.model.Transfer;

/**
 *
 * @author Sergey.Bychkov
 */
public class MSelect extends TwinColSelect {

    private static final long serialVersionUID = 1L;

    public MSelect() {
        super.setWidth("100%");
    }

    public List<Transfer> getTrans() {
        ArrayList<Transfer> result = new ArrayList<>();
         Object  value = getValue();
        if (value !=null ){
            result.addAll((Collection<? extends Transfer>) value);
        }
       return result;
    }

    public void setData(List<Transfer> list) {
        setContainerDataSource(new BeanItemContainer<>(Transfer.class, list));
    }
}
