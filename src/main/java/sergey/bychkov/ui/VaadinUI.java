/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sergey.bychkov.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.Responsive;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import sergey.bychkov.model.FondReport;
import sergey.bychkov.model.Partner;
import sergey.bychkov.model.PostOffice;
import sergey.bychkov.model.Transfer;
import sergey.bychkov.service.IAggregator;
import sergey.bychkov.ui.widget.MGrid;
import sergey.bychkov.ui.widget.MSelect;
import utils.PdfGenerator;

/**
 *
 * @author Sergey.Bychkov
 */
@SpringUI()
@Theme("valo")
public class VaadinUI extends UI {

    private static final long serialVersionUID = 1L;

    private Panel p;
    @Autowired
    private IAggregator agg;
    private ComboBox postOffice;
    private ComboBox partner;
    private Panel listPanel;
    private MSelect transSelect;
    private Button start;
    private Button print;
    private BrowserWindowOpener opener;
    private MGrid grid;

    @Override
    protected void init(VaadinRequest request) {
        VerticalLayout vl = new VerticalLayout();
        listPanel = new Panel();

        listPanel.setVisible(false);
        transSelect = new MSelect();

        listPanel.setContent(transSelect);

        p = new Panel();
        p.setSizeFull();

        HorizontalLayout hl = new HorizontalLayout();

        partner = new ComboBox("Контрагент",
                new BeanItemContainer<>(Partner.class, agg.getPartner()));

        postOffice = new ComboBox("Почтамт",
                new BeanItemContainer<>(PostOffice.class, agg.getPostOffice()));
        postOffice.addValueChangeListener((v) -> {
            postSelected();
        });
        hl.addComponent(postOffice);
        Label l = new Label("");
        l.setWidth("100%");
        hl.addComponent(l);
        start = new Button("Старт", e -> start());
        start.setHeight("100%");
        hl.addComponent(start);
        print = new Button("Печатная форма");
        print.setEnabled(false);
        print.setHeight("100%");
        hl.addComponent(print);
        hl.setExpandRatio(l, 1);
        hl.setComponentAlignment(print, Alignment.MIDDLE_RIGHT);
        hl.setComponentAlignment(start, Alignment.MIDDLE_RIGHT);
        vl.addComponent(hl);
        vl.addComponent(listPanel);
        vl.addComponent(p);
        vl.setExpandRatio(p, 1);
        vl.setSizeFull();
        Responsive.makeResponsive(vl);
        setContent(vl);        
    }

    private void start() {

        grid = new MGrid();
        Long paId = partner.getValue() != null ? ((Partner) partner.getValue()).getId() : null;
        Long poId = postOffice.getValue() != null ? ((PostOffice) postOffice.getValue()).getId() : null;
        List<Transfer> transfers = transSelect.getTrans();
        List<FondReport> data = agg.getData(poId, paId, transfers);
        grid.setData(data);
        p.setContent(grid);
        print(data);
    }

    private void print(List<FondReport> data) {
        List<Transfer> transfers = transSelect.getTrans();

        StreamSource source = () -> new ByteArrayInputStream(PdfGenerator.generateFond(data, transfers));
        String filename = "report" + (new Date()).getTime() + ".pdf";
        StreamResource resource = new StreamResource(source, filename);
        resource.setMIMEType("application/pdf");
        resource.getStream().setParameter("Content-Disposition",
                "attachment; filename=" + filename);

        if (opener == null) {
            opener = new BrowserWindowOpener(resource);
            opener.extend(print);
        } else {
            opener.setResource(resource);
        }
        print.setEnabled(true);
    }

    private void postSelected() {
        listPanel.setVisible(true);
        Long poId = postOffice.getValue() != null ? ((PostOffice) postOffice.getValue()).getId() : null;
        transSelect.setData(agg.getTransfers(poId, null));
        print.setEnabled(false);

    }

}
