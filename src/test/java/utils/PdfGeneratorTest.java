/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sergey.bychkov.model.FondReport;
import sergey.bychkov.model.Transfer;

/**
 *
 * @author Sergey.Bychkov
 */
public class PdfGeneratorTest {
    
    public PdfGeneratorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of generate method, of class PdfGenerator.
     */
    @Test
    public void testGenerate() {
        System.out.println("generate");
        String header = "";
        String footer = "";
        List data = null;
        float[] sizes = null;
        byte[] expResult = null;
        byte[] result = PdfGenerator.generate(header, footer, data, null,null);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of generateFond method, of class PdfGenerator.
     */
    @Test
    public void testGenerateFond() {
        System.out.println("generateFond");
        List<FondReport> data = null;
        List<Transfer> trans = null;
        byte[] expResult = null;
        byte[] result = PdfGenerator.generateFond(data, trans);
        assertArrayEquals(expResult, result);
    }
    
}
