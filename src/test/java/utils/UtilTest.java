/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sergey.bychkov.model.Transfer;

/**
 *
 * @author Sergey.Bychkov
 */
public class UtilTest {
    
    public UtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMaxDate method, of class Util.
     */
    @Test
    public void testGetMaxDate() {
        System.out.println("getMaxDate");
        List<Transfer> data = null;
        String expResult = "";
        String result = Util.getMaxDate(data);
        assertEquals(expResult, result);
        data = new ArrayList<>();
        Date today = new Date();
        data.add(new Transfer(1L, today, today));
        data.add(new Transfer(2L, new Date(0L), new Date(0L)));
        result = Util.getMaxDate(data);
        expResult = Util.sdf(today);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMinDate method, of class Util.
     */
    @Test
    public void testGetMinDate() {
        System.out.println("getMinDate");
        List<Transfer> data = null;
        String expResult = "";
        String result = Util.getMinDate(data);
        assertEquals(expResult, result);
        data = new ArrayList<>();
        data.add(new Transfer(1L, new Date(), new Date()));
        data.add(new Transfer(2L, new Date(0L), new Date(0L)));
        result = Util.getMinDate(data);
        expResult = "01.01.1970";
        assertEquals(expResult, result);
        
    }

    /**
     * Test of sdf method, of class Util.
     */
    @Test
    public void testSdf() {
        System.out.println("sdf");
        Date date = null;
        String expResult = "";
        String result = Util.sdf(date);
        assertEquals(expResult, result);
        
    }
    
}
